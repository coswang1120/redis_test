import redis
import time

pool = redis.ConnectionPool(host='localhost', port=6379, decode_responses=True)
r = redis.Redis(connection_pool=pool)

print(r.set('fruit', 'watermelon', nx=True))   
print((r.set('fruit', 'watermelon', xx=True))) 
print(r.setnx('fruit1', 'banana')) 





 

keydict = {}
keydict['key1'] = 324
keydict['key2'] = 'ag'
print (r.mset(keydict))
list = ['key1','key2']
print (r.mget(list))
print(r.getset("food", "barbecue"))

r.set("cn_name", "0123")
print(r.getrange("cn_name", 0, -1)) 

r.set("junxi", "junxi")
r.setrange("junxi", 1, "ccc")
print(r.get("junxi"))



source = "foo"
for i in source:
    num = ord(i)
print (bin(num).replace('b',''))


r.set("foo", 123)
print(r.mget("foo", "foo1", "foo2", "k1", "k2"))
r.incr("foo1", amount=1)
print(r.mget("foo", "foo1", "foo2", "k1", "k2"))

r.set("name", "junxi")
r.append("name", "haha")    
print(r.get("name"))



r.hset("hash1", "k1", "v1")
r.hset("hash1", "k2", "v2")
print(r.hkeys("hash1")) 
print(r.hget("hash1", "k1"))   
print(r.hmget("hash1", "k1", "k2"))
r.hsetnx("hash1", "k2", "v3") 


r.lpush("list1", 11, 22, 33)
print(r.lrange('list1', 0, -1))

r.rpush("list2", 44, 55, 66)   
print(r.llen("list2")) 
print(r.lrange("list2", 0, -1))


r.sadd("set1", 33, 44, 55, 66)  
print(r.scard("set1")) 
print(r.smembers("set1"))   
